{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}

module Examples where

import Lattice
import DCC

example1 :: T L Bool -> T H Bool
example1 t = t >>>= return

--example2 :: T H Int -> (T L Int, T H Int)
--example2 secret = secret >>>= \n -> (return 42, return (n+1))

example3 :: T L Int -> (T L Int, T H Int)
example3 public = public >>>= \n -> (return 42, return (n+1))
