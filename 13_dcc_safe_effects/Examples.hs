{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
module Examples where

import Lattice
import DCC

-- The attack does not work anymore
attack :: T H Bool -> T H (Eff L ())
attack tb =
  tb >>>= \b -> return $  if b then printL "True"
                          else printL "False"

exAttack = runEff $ dist (attack (return True))
