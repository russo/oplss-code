{-# LANGUAGE Trustworthy           #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances  #-}

module DCC
  (
    -- types
    T ()
    -- superbind
  , (>>>=)
    -- For effects
  , MonadT (liftT)
  , type ProtectedAt
    -- IO and T l a
  , Eff ()
  , type Join
  -- graded monad
  , returnEff
  , bindEff
  -- combination pure and effectful
  , dist
  -- effects
  , printL
  , printH
  -- execution of effects
  , runEff
  )

where

import Control.Monad
import Control.Monad.Trans

import Lattice

-- The `T` monad family from the DCC paper
newtype T (l :: Lattice) a = T { unT :: a }

-- Conjunction
type family x && y where
  True  && True  = True
  False && b     = False
  b     && False = False

{- Notion of protected at -}
type family ProtectedAt (t :: *) (l :: Lattice) where
  ProtectedAt (T l' a) l = l <= l'

  -- This diverges from the original DCC calculus,
  -- this is not a problem as () can not transmit any
  -- information
  ProtectedAt ()       l = True

  -- This line requires `UndecidableInstances`, but since the type family is closed
  -- and _obviously_ total, we don't need to worry!
  ProtectedAt (s, t) l   = (ProtectedAt s l) && (ProtectedAt t l)
  ProtectedAt (s -> t) l = ProtectedAt t l
  ProtectedAt s l = False

-- The bind from the DCC paper
infixl >>>=

-- Superbind
(>>>=) :: (s `ProtectedAt` l) ~ True => T l a -> (a -> s) -> s
t >>>= f = f (unT t)

-- T is most surely a monad
instance Monad (T l) where
  return = T
  (>>=)  = (>>>=)

instance Applicative (T l) where
  pure  = return
  (<*>) = ap

instance Functor (T l) where
  fmap = liftM

class MonadT mt l where
  liftT :: T l a -> mt a

instance MonadT (T l) l where
  liftT = id

instance (MonadT inner l, Monad inner, MonadTrans mt) => MonadT (mt inner) l where
  liftT = lift . liftT

instance Show a => Show (T L a) where
  show ta = "T L " ++ show (unT ta)

instance Show a => Show (T H a) where
  show ta = "T H " ++ "***"

----------
-- Effects
----------

-- Type-level join
type family Join (l1 :: Lattice) (l2 :: Lattice) :: Lattice where
  Join _ H = H
  Join H _ = H
  Join L L = L

newtype Eff (l :: Lattice) a = Eff { io :: IO a }

instance Functor (Eff l) where
  fmap f = Eff . fmap f . io

returnEff :: a -> Eff L a
returnEff = Eff . return

bindEff :: Eff l1 a -> (a -> Eff l2 b) -> Eff (Join l1 l2) b
bindEff ma k = Eff $ io ma >>= \a -> io (k a)

dist :: (l1 <= l2) ~ True => T l1 (Eff l2 a) -> Eff l2 (T l1 a)
dist = fmap return . unT

runEff :: Eff l a -> IO a
runEff = io

-- Supported IO effect: IO Output
printL :: String -> Eff L ()
printL = Eff . putStrLn

printH :: String -> Eff H ()
printH = Eff . putStrLn

-- Supported IO effect: IO References
-- ?

-- Supported IO effect: IO Exceptions
-- ?
