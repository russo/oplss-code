{-# LANGUAGE Trustworthy #-}
{-# LANGUAGE TypeApplications #-}
module MACWget where

{-- Security library modules --}
import Lattice
import TCB

import Network.HTTP.Wget

wgetMAC :: String -> MAC L (Maybe String)
wgetMAC s = ioTCB (wget @Maybe s [] [])
