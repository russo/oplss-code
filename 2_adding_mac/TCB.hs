{-# LANGUAGE Unsafe #-}
module TCB
    (
       L ()
     , H ()
     -- Labeled expressions
     , Labeled (LabeledTCB)
     , label
     , unlabel
     -- Monad MAC
     , MAC (MkMAC)
     , runMAC
     -- TCB operations
     , ioTCB
    )

where

import Lattice
import Control.Applicative

{-- Labeled terms --}
newtype Labeled l a = LabeledTCB a

label   :: Less l l' => a -> MAC l (Labeled l' a)
unlabel :: Less l' l => Labeled l' a -> MAC l a

{-- MAC's operations --}
newtype MAC l a = MkMAC (IO a)
runMAC :: MAC l a -> IO a

{-- TCB operations --}
ioTCB :: IO a -> MAC l a

{---------------------}
{---------------------}
{-- Implementations --}
{---------------------}
{---------------------}

label v = return (LabeledTCB v)
unlabel (LabeledTCB v) = return v

instance Functor (MAC l) where
    fmap f (MkMAC io) = MkMAC (fmap f io)

instance Applicative (MAC l) where
    pure = MkMAC . return
    (<*>) (MkMAC f) (MkMAC a) = MkMAC (f <*> a)

instance Monad (MAC l) where
   return = pure
   MkMAC m >>= k = ioTCB (m >>= runMAC . k)

ioTCB = MkMAC

runMAC (MkMAC m) = m
