#!/bin/sh


directories=$(find . -type d -maxdepth 1 | tail -n +2)

for dir in $directories 
do
  cd $dir 
  cat *.hs | pygmentize | aha > temp.html && wkhtmltopdf temp.html ../${dir}.pdf && rm -f temp.html
  cd ..
done

