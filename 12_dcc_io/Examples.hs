{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Examples where

import Lattice
import DCC

attack :: T H Bool -> T H (IO ())
attack tb =
  tb >>>= \b -> return $  if b then putStrLn "True"
                          else putStrLn "False"

exAttack = secureIO (attack (return True))
