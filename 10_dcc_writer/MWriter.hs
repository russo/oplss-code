{-# LANGUAGE Safe #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}

module MWriter where

import Lattice
import DCC
import Control.Monad.Writer

type Outputs o =  (T L o, T H o)

class Project (l :: Lattice) where
  type OutputsAbove l o
  project  :: Outputs o -> (T l o, OutputsAbove l o)
  merge    :: (T l o, OutputsAbove l o) -> Outputs o -> Outputs o

instance Project H where
  type OutputsAbove H o = ()
  project (_, h)         = (h, ())
  merge   (h, _) (l, _)  = (l, h)

instance Project L where
  type OutputsAbove L o = T H o
  project (l, h)   = (l, h)
  merge   (l, h) _ = (l, h)

fill' :: (Project l, Monoid (OutputsAbove l o), Monoid o)
      => T l o -> Outputs o
fill' p = merge (p, mempty) mempty

fill :: (Project l, Monoid o)
     => (T l o, OutputsAbove l o) -> Outputs o
fill p = merge p mempty

teller
  :: (Project l,
      Monoid o,
      Monoid (OutputsAbove l o))
  => T l o -> WT o h ()
teller t = tell (fill' t)

type WT o l a = WriterT (Outputs o) (T l) a -- T l (a, Outputs o)

runWT :: WT o l a -> T l (a, Outputs o)
runWT = runWriterT

-- Orphan instances but no problem!
instance Semigroup s => Semigroup (T l s) where
  (<>) = liftM2 (<>)

instance Monoid m => Monoid (T l m) where
  mempty   = return mempty
  mappend  = liftM2 mappend
