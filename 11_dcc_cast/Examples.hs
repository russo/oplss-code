{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Examples where

import Lattice
import DCC
import MWriter

example1 :: T L Bool -> T H Bool
example1 t = t >>>= return

--example2 :: T H Int -> (T L Int, T H Int)
--example2 secret = secret >>>= \n -> (return 42, return (n+1))

example3 :: T L Int -> (T L Int, T H Int)
example3 public = public >>>= \n -> (return 42, return (n+1))

-- Computations with outputs
example4 :: WT String L ()
example4 = do
  teller msg1
  teller msg2
  teller msg3
  return ()
  where msg1 :: T L String
        msg1 = return "Hello!"

        msg2 :: T H String
        msg2 = return "Secret 42"

        msg3 :: T L String
        msg3 = return " and bye!"

runEx4 = runWT example4

example5 :: T H Int -> WT String L ()
example5 sec = do
  teller msg1
  let sec' :: T H String = sec >>>= \n -> return $ show (n+1)
  teller sec'
  return ()
  where msg1 :: T L String
        msg1 = return "Hello!"

runEx5 = runWT (example5 (return 42))


-- Do you see a problem here? (label creep)
example6 :: T H Int ->  T L Int -> WT String H ()
example6 sec pub = do
  teller (fmap show pub)
  let sec' :: T H String = sec >>>= \n -> return $ show (n+1)
  teller sec'
  return ()

runEx6 = runWT (example6 (return 42) (return 0))


example7 :: T H Int ->  T L Int -> WT String L ()
example7 sec pub = do
  teller (fmap show pub)
  cast $ example6 sec pub
  teller (fmap show pub)
  return ()

runEx7 = runWT (example7 (return 42) (return 0))
