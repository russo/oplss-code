const http = require('http');
const qstr = require('querystring');
const PORT = 8080;

const handler = function (req, res) {
  console.log('Requested URL: %s', req.url);
  res.statusCode = 200;
  res.end("Ok");
}

http.createServer(handler).listen(PORT);
console.log('Server listening on http://localhost:%s', PORT);
