module Intro where

plusone :: Int -> Int
plusone x = x + 1

-- Parametric polymorphism
first :: (a,b) -> a
first (x,y) = x

lst1 :: [Int]
lst1 = [1,3,5]

lst2 :: [Int]
lst2 = 7:[]

lst3 = lst1 ++ lst2


-- Algebraic data types
data Nationality = Argentinian | Italian

favorite_food :: Nationality -> String
favorite_food Argentinian = "Asado"
favorite_food Italian     = "Pasta"

-- Algebraic data types based on built-in types
data TreeI = LeafI Int | NodeI TreeI TreeI
             deriving Show

exTreeI :: TreeI
exTreeI = NodeI (NodeI (LeafI 42) (LeafI 10)) (LeafI 100)

data TreeS = LeafS String | NodeS TreeS TreeS
             deriving Show

exTreeS :: TreeS
exTreeS = NodeS (NodeS (LeafS "Hello") (LeafS "Hej")) (LeafS "Ciao")

-- Polymorphic ADTs

data Tree a = Leaf a | Node (Tree a) (Tree a)
                       deriving Show

exTreeInt :: Tree Int
exTreeInt = Node (Node (Leaf 42) (Leaf 10)) (Leaf 100)


exTreeStr :: Tree String
exTreeStr = Node (Node (Leaf "Hello") (Leaf "Hej")) (Leaf "Ciao")

-- Functions on polymorphic ADTs
flatten :: Tree a -> [a]
flatten (Leaf a)   = [a]
flatten (Node l r) = flatten l ++ flatten r
