module IntroEffects where


greetings :: IO ()
greetings = do
  putStrLn "Welcome! What's your name?"
  name <- getLine
  putStrLn ("Welcome " ++ name ++ "!")

ask_fullname :: IO String
ask_fullname = do
  putStrLn "What's your first name?"
  name <- getLine
  putStrLn "What's your last name?"
  last <- getLine
  return (name ++ " " ++ last)

welcome :: IO ()
welcome = do
  putStrLn "Welcome to the system!"
  fullname <- ask_fullname
  putStrLn $ "Hello " ++ fullname ++ "!"

-- IO computations are descriptions
pure1 = let x = putStrLn "Hello!" in 42
pure2 = let x = putStrLn "Hello!" in (42,x)
