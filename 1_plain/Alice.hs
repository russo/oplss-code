module Alice where

import qualified Bob

manager :: IO String
manager = do
  putStr "Please, select your password:"
  pass <- getLine
  bool <- Bob.common_pwd pass
  if bool then do putStrLn "Your password is too common!"
                  manager
  else return pass
