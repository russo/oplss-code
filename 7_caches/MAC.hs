{-# LANGUAGE Trustworthy #-}

module MAC
    (
       H ()
     , L ()
     -- It comes from Core
     , Labeled ()
     , label
     , unlabel
     -- Monad MAC
     , MAC ()
     , runMAC
     --, joinMAC
     , forkMAC
     -- Exceptions
     , throwMAC
     , catchMAC
     -- Auxiliary proxies
     , fix
     -- MVars
     , newMACMVar
     , newMACEmptyMVar
     , takeMACMVar
     , putMACMVar
     -- Delaying a thread
     , threadDelayMAC
     -- print
     , printMAC
     -- auxiliary
     , timed
    )

where

import TCB

-- | To help the type-system
fix :: l -> MAC l ()
fix _l = return ()
