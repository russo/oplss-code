{-# LANGUAGE Unsafe #-}

module TCB
    (
       L ()
     , H ()
     -- Labeled expressions
     , Labeled (LabeledTCB)
     , label
     , unlabel
     -- Monad MAC
     , MAC (MkMAC)
     , runMAC
     -- , joinMAC
     , forkMAC
     -- Exceptions
     , throwMAC
     , catchMAC
     -- MVars
     , newMACMVar
     , newMACEmptyMVar
     , takeMACMVar
     , putMACMVar
     -- Delaying a thread
     , threadDelayMAC
     -- TCB operations
     , ioTCB
     , timed
     , printMAC
    )

where

import Control.Applicative
import Control.Exception
import Control.Concurrent
import Control.Concurrent.MVar
import Data.Time.Clock


{-- Security levels --}
import Lattice


{-- Labeled values --}
newtype Labeled l a = LabeledTCB a
type MACMVar l a = Labeled l (MVar a)

label      :: Less l l' => a -> MAC l (Labeled l' a)
unlabel    :: Less l' l => Labeled l' a -> MAC l a

{-- MAC's operations --}
newtype MAC l a = MkMAC (IO a)
runMAC  :: MAC l a -> IO a
joinMAC :: (Less l l') => MAC l' a -> MAC l (Labeled l' a)

{-- TCB operations --}
ioTCB      :: IO a -> MAC l a

{-- Exceptions --}
throwMAC :: Exception e => e -> MAC l a
catchMAC :: Exception e => MAC l a -> (e -> MAC l a) -> MAC l a

{-- MVars --}

newMACMVar      :: Less l l' => a -> MAC l (MACMVar l' a)
newMACEmptyMVar :: Less l l' => MAC l (MACMVar l' a)
takeMACMVar     :: Less l l => MACMVar l a -> MAC l a
putMACMVar      :: Less l l => MACMVar l a -> a -> MAC l ()




{---------------------}
{---------------------}
{-- Implementations --}
{---------------------}
{---------------------}

label v = return (LabeledTCB v)

unlabel (LabeledTCB v) = return v




instance Functor (MAC l) where
    fmap f (MkMAC io) = MkMAC (fmap f io)

instance Applicative (MAC l) where
    pure = MkMAC . return
    (<*>) (MkMAC f) (MkMAC a) = MkMAC (f <*> a)

instance Monad (MAC l) where
   return = pure
   MkMAC m >>= k = ioTCB (m >>= runMAC . k)

ioTCB = MkMAC

runMAC (MkMAC m) = m


joinMAC m = ioTCB (runMAC (catch_any_exception m))
            where
                  catch_any_exception m' =
                       catchMAC
                                (do v <- m'
                                    return (LabeledTCB v))
                                hd

                  hd :: SomeException -> MAC l (Labeled l' a)
                  hd e = return (LabeledTCB (throw e))

forkMAC :: Less l l' => MAC l' () -> MAC l ()
forkMAC m = (ioTCB . forkIO . runMAC) m >> return ()

throwMAC = ioTCB . throw
catchMAC (MkMAC io) hd = ioTCB ( catch io (runMAC . hd) )

newMACMVar v      = ioTCB (newMVar v) >>= label
newMACEmptyMVar   = ioTCB newEmptyMVar >>= label
takeMACMVar lmv   = unlabel lmv >>= ioTCB . takeMVar
putMACMVar  lmv v = unlabel lmv >>= \mv -> ioTCB $ putMVar mv v

-- Delaying a thread
threadDelayMAC = ioTCB . threadDelay

-- | Converts seconds to milliseconds.
secsToMillis :: NominalDiffTime -> Double
secsToMillis t = realToFrac t * (10^(3 :: Int))

timed m = do tt <- ioTCB $ getCurrentTime
             r  <- m
             if r == r then
                        do tt' <- ioTCB $ getCurrentTime
                           ioTCB (print (secsToMillis (diffUTCTime tt' tt)))
                           ioTCB (print "--x--")
                           return r
                       else return r

-- Public print
printMAC :: String -> MAC L ()
printMAC str = ioTCB (putStrLn str)
