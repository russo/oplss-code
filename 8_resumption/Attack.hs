{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE Safe #-}
{-# LANGUAGE TypeApplications #-}

module Main where

{-- Security levels --}
import Lattice
import MAC
import Control.Monad (when)
import GHC.Int

import Data.Array.Unboxed

type LArray l = Labeled l (UArray Int32 Int32)

sizeArray = 4194304 * 2 -- 32m L3 cache

bigArray :: UArray Int32 Int32
bigArray = array (0,sizeArray-1) (zip [0..sizeArray-1] (repeat 42))

bigArray2 :: UArray Int32 Int32
bigArray2 = array (0,sizeArray-1) (zip [0..sizeArray-1] (repeat 150))

secret :: Less l H => Res MAC l (Labeled H Int32)
secret = label 1

lowArray :: Res MAC L (LArray L)
lowArray = label bigArray

highArray :: Res MAC H (LArray H)
highArray = label bigArray2

attack :: Res MAC L ()
attack = do
  mvar <- newMACEmptyMVar
  lowA <- lowArray
  arr  <- unlabel lowA -- fill in the cache with the low array
  b   <- travel arr
  when b $
     forkMAC SH $ do sec <- secret @H
                     s <- unlabel sec
                     when (s == 0) $ do highA <- highArray
                                        arrH  <- unlabel highA
                                        b     <- travel arrH
                                        when b $ return ()
                     return ()
  -- wait for some time
  threadDelayMAC 2000000
  -- Starting the race
  printMAC "Starting the race!"
  forkMAC SL $ do b <- travel arr
                  when b $ putMACMVar mvar True

  forkMAC SL $ do threadDelayMAC 5500000
                  putMACMVar mvar False

  g <- takeMACMVar mvar
  _ <- takeMACMVar mvar
  printMAC $ show g
  return ()

travel arr = return $ arr == arr
                      &&
                      arr <= arr
                      && arr >= arr
                      && (not (arr > arr))

main :: IO ()
main = sch L [attack] []
