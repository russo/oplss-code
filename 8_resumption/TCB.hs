{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE Unsafe #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE AllowAmbiguousTypes #-}


module TCB
    (
       L ()
     , H ()
     -- Labeled expressions
     , Labeled (LabeledTCB)
     , label
     , unlabel
     -- Monad MAC
     , MAC (MkMAC)
     , runMAC
     -- , joinMAC
     , forkMAC
     -- Exceptions
     , throwMAC
     , catchMAC
     -- MVars
     , newMACMVar
     , newMACEmptyMVar
     , takeMACMVar
     , putMACMVar
     -- Delaying a thread
     , threadDelayMAC
     -- TCB operations
     , ioTCB
     , printMAC
     -- Atom
     , Res ()
     , atom
     , SLevel (..)
     , Level (..)
     , sch
    )

where

import Control.Applicative
import Control.Exception
import Control.Concurrent
import Control.Concurrent.MVar
import Data.Time.Clock


{-- Security levels --}
import Lattice


{-- Labeled values --}
newtype Labeled l a = LabeledTCB a
type MACMVar l a = Labeled l (MVar a)

label      :: Less l l' => a -> Res MAC l (Labeled l' a)
unlabel    :: Less l' l => Labeled l' a -> Res MAC l a

{-- MAC's operations --}
newtype MAC l a = MkMAC (IO a)
runMAC  :: MAC l a -> IO a
joinMAC :: (Less l l') => MAC l' a -> MAC l (Labeled l' a)

{-- TCB operations --}
ioTCB      :: IO a -> MAC l a

{-- Exceptions --}
throwMAC :: Exception e => e -> MAC l a
catchMAC :: Exception e => MAC l a -> (e -> MAC l a) -> MAC l a

{-- MVars --}

newMACMVar      :: Less l l' => a -> Res MAC l (MACMVar l' a)
newMACEmptyMVar :: Less l l' => Res MAC l (MACMVar l' a)
takeMACMVar     :: Less l l => MACMVar l a -> Res MAC l a
putMACMVar      :: Less l l => MACMVar l a -> a -> Res MAC l ()

{---------------------}
{---------------------}
{-- Implementations --}
{---------------------}
{---------------------}

label v = atom $ return (LabeledTCB v)

unlabel (LabeledTCB v) = atom $ return v


instance Functor (MAC l) where
    fmap f (MkMAC io) = MkMAC (fmap f io)

instance Applicative (MAC l) where
    pure = MkMAC . return
    (<*>) (MkMAC f) (MkMAC a) = MkMAC (f <*> a)

instance Monad (MAC l) where
   return = pure
   MkMAC m >>= k = ioTCB (m >>= runMAC . k)

ioTCB = MkMAC

runMAC (MkMAC m) = m


joinMAC m = ioTCB (runMAC (catch_any_exception m))
            where
                  catch_any_exception m' =
                       catchMAC
                                (do v <- m'
                                    return (LabeledTCB v))
                                hd

                  hd :: SomeException -> MAC l (Labeled l' a)
                  hd e = return (LabeledTCB (throw e))

forkMAC :: Less l l' => SLevel l' -> Res MAC l' () -> Res MAC l ()
forkMAC l r1 = Fork l r1 (return ())

throwMAC = ioTCB . throw
catchMAC (MkMAC io) hd = ioTCB ( catch io (runMAC . hd) )

newMACMVar v      = Atom $ do mv <- ioTCB (newMVar v)
                              return (label mv)

newMACEmptyMVar   = Atom $ do mv <- ioTCB newEmptyMVar
                              return (label mv)
takeMACMVar lmv   = do
  mv <- unlabel lmv
  maybe <- atom $ ioTCB $ tryTakeMVar mv
  case maybe of
       Nothing -> takeMACMVar lmv
       Just a  -> return  a

putMACMVar  lmv v = do
  mv   <- unlabel lmv
  bool <- atom $ ioTCB $ tryPutMVar mv v
  if bool then return ()
          else putMACMVar lmv v

-- Delaying a thread
threadDelayMAC = atom . ioTCB . threadDelay

-- Public print
printMAC :: String -> Res MAC L ()
printMAC str = atom $ ioTCB (putStrLn str)

{-- Resumptions --}

data SLevel l where
  SL :: SLevel L
  SH :: SLevel H

data Res m l a where
  Done :: a -> Res m l a
  Atom :: m l (Res m l a) -> Res m l a
  Fork :: Less l l' => SLevel l' -> Res m l' () -> Res m l a -> Res m l a

atom :: Monad (m l) => m l a -> Res m l a
atom m = Atom $ m >>= return . Done

data Level = L | H

sch :: Level -> [Res MAC L ()] -> [Res MAC H ()] -> IO ()

sch _ [] [] = return ()
sch L [] hs = sch H [] hs
sch H ls [] = sch L ls []

sch L (Done () : rs) hs = sch H rs hs
sch H ls (Done () : rs) = sch L ls rs

sch L (Atom step : rs) hs  = do
     r <- runMAC $ step
     sch H (rs ++ [r]) hs

sch H ls (Atom step : rs) = do
     r <- runMAC $ step
     sch L ls (rs ++ [r])

sch L (Fork SL r1 r2 : rs) hs = do
  sch H (rs ++ [r1,r2]) hs

sch L (Fork SH r1 r2 : rs) hs = do
  sch H (rs ++ [r2]) (hs ++ [r1])

sch H ls (Fork SH r1 r2 : rs) = do
  sch L ls (rs ++ [r1,r2])

instance Functor (m l) => Functor (Res m l) where
  fmap f (Done a)       = Done (f a)
  fmap f (Atom m)       = Atom (fmap (fmap f) m)
  fmap f (Fork l r1 r2) = Fork l r1 (fmap f r2)

instance Applicative (m l) => Applicative (Res m l) where
  pure = Done
  (Done f)       <*> xx = f <$> xx
  (Atom mf)      <*> xx = Atom $ fmap (\rr -> rr <*> xx) mf
  (Fork l r1 r2) <*> xx = Fork l r1 (r2 <*> xx)

instance Monad (m l) => Monad (Res m l) where
    return = Done
    (Done x) >>= k = k x
    (Atom m) >>= k = Atom $ do res <- m
                               return (res >>= k)
    Fork l r1 r2 >>= k = Fork l r1 (r2 >>= k)
