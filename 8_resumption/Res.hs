{-# LANGUAGE Unsafe #-}
{-# LANGUAGE GADTs #-}

module Res where

import Control.Monad

{-- Resumptions -}

data Res m a where
  Done :: a -> Res m a
  Atom :: m (Res m a) -> Res m a

instance Functor m => Functor (Res m) where
  fmap f (Done a)     = Done (f a)
  fmap f (Atom m)     = Atom (fmap (fmap f) m)

instance Applicative m => Applicative (Res m) where
  pure = Done
  (Done f)     <*> xx = f <$> xx
  (Atom mf)    <*> xx = Atom $ fmap (\rr -> rr <*> xx) mf

instance Monad m => Monad (Res m) where
    return = Done
    (Done x) >>= k = k x
    (Atom m) >>= k = Atom $ do res <- m
                               return (res >>= k) -- Res m b

sch :: Monad m => [Res m ()] -> m ()
sch []                 = return ()
sch (Done ()    : rs)  = sch rs
sch (Atom step  : rs)  = do
  r <- step
  sch (rs ++ [r])

atom :: Monad m => m a -> Res m a
atom m = Atom $ m >>= return . Done

p1 = sequence_ $ replicate 10 (putStr "A")
p2 = sequence_ $ replicate 10 (putStr "B")

main = do
  p1
  p2


p1' = replicate 10 (do atom $ putStr "A"
                       atom $ putStr "C"
                   )
p2' = replicate 10 (atom $ putStr "B")

main2 = sch $ concat $ zipWith (\a b -> [a,b]) p1' p2'
