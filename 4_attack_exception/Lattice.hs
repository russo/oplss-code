{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Safe #-}

-- | Encodes a security lattice.
module Lattice
    (
      Less ()
    , H ()
    , L ()
    )
where


-- | Label for public data
data L = MkL
-- | Label for secrets
data H = MkH

-- Pablo's trick to avoid instances
-- Define a super-class
-- | Type class used to avoid arbitrary instances by attackers (Pablo's trick)
class Less l l' where

-- | Type class encoding security lattices
instance Less L L where
instance Less L H where
instance Less H H where
